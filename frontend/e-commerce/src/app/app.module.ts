import {APP_INITIALIZER, NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {ProductListComponent} from "./components/product-list/product-list.component";
import {HttpClientModule} from "@angular/common/http";
import {ProductService} from "./services/product.service";
import {RouterModule, Routes} from "@angular/router";
import {ProductCategoryMenuComponent} from "./components/product-category-menu/product-category-menu.component";
import {SearchComponent} from "./components/search/search.component";
import {ProductCategoryService} from "./services/product-category.service";
import {ProductDetailsComponent} from "./components/product-details/product-details.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {CartStatusComponent} from "./components/cart-status/cart-status.component";
import {CartDetailsComponent} from "./components/cart-details/cart-details.component";
import {CheckoutComponent} from "./components/checkout/checkout.component";
import {ReactiveFormsModule} from "@angular/forms";
import {LoginStatusComponent} from './components/login-status/login-status.component';
import {MembersPageComponent} from './components/members-page/members-page.component';
import {AuthGuard} from "./guards/auth.guard";
import {initializer} from "./utils/initializer";
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";
import {OrderHistoryComponent} from './components/order-history/order-history.component';


const routes: Routes = [
  {path: "checkout", component: CheckoutComponent},
  {path: "cart-details", component: CartDetailsComponent},
  {path: "products/:id", component: ProductDetailsComponent},
  {path: "search/:keyword", component: ProductListComponent},
  {path: "category/:id", component: ProductListComponent},
  {path: "category", component: ProductListComponent},
  {path: "products", component: ProductListComponent},
  {path: "order-history", component: OrderHistoryComponent, canActivate: [AuthGuard]},
  {path: "members", component: MembersPageComponent, canActivate: [AuthGuard]},
  {path: "", redirectTo: "/products", pathMatch: "full"},
  {path: "**", redirectTo: "/products", pathMatch: "full"},
];

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ProductCategoryMenuComponent,
    SearchComponent,
    ProductDetailsComponent,
    CartStatusComponent,
    CartDetailsComponent,
    CheckoutComponent,
    LoginStatusComponent,
    MembersPageComponent,
    OrderHistoryComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    KeycloakAngularModule
  ],
  providers: [ProductService, ProductCategoryService
    , {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
