import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product.service";
import {Product} from "../../common/product";
import {ActivatedRoute} from "@angular/router";
import {CartService} from "../../services/cart.service";
import {CartItem} from "../../common/cart-item";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: Product[];
  currentCategoryId: number;
  private previousCategoryId: number;
  searchMode: Boolean;
  pageNumber: number;
  pageSize: number;
  totalElements: number;
  maxSize: number;
  private previousKeyword: string;

  constructor(private productService: ProductService, private cartService: CartService, private route: ActivatedRoute) {
    this.products = [];
    this.currentCategoryId = 1;
    this.previousCategoryId = 1;
    this.searchMode = false;
    this.pageNumber = 1;
    this.pageSize = 8;
    this.totalElements = 0;
    this.maxSize = 5;
    this.previousKeyword = "";
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
        this.listProducts();
      }
    );
  }

  listProducts() {
    this.searchMode = this.route.snapshot.paramMap.has("keyword");
    if (this.searchMode)
      this.handleSearchProducts();
    else {
      this.handleListProducts();
    }
  }

  processResult() {
    // @ts-ignore
    return data => {
      this.products = data._embedded?.products;
      this.pageNumber = data.page?.number + 1;
      this.pageSize = data.page?.size;
      this.totalElements = data.page?.totalElements;
    };
  }

  private handleListProducts() {

    const hasCategoryId: Boolean = this.route.snapshot.paramMap.has("id");

    if (hasCategoryId)
      this.currentCategoryId = +this.route.snapshot.paramMap.get('id')!;

    if (this.previousCategoryId != this.currentCategoryId)
      this.pageNumber = 1;
    this.previousCategoryId = this.currentCategoryId;
    this.productService.getProductListPaginate(this.currentCategoryId, this.pageNumber - 1, this.pageSize).subscribe(
      this.processResult());
  }

  private handleSearchProducts() {
    const keyword: string = this.route.snapshot.paramMap.get("keyword")!;
    if (this.previousKeyword != keyword)
      this.pageNumber = 1;
    this.previousKeyword = keyword;
    this.productService.searchForProductsPaginate(keyword, this.pageNumber - 1, this.pageSize).subscribe(
      this.processResult()
    );
  }

  updatePageSize(pageSize: string) {
    this.pageNumber = 1;
    this.pageSize = Number(pageSize);
    this.listProducts();
  }

  addToCart(product: Product) {
    const cartItem = new CartItem(product);
    this.cartService.addToCart(cartItem);
  }
}
