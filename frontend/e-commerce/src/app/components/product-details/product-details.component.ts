import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product.service";
import {ActivatedRoute, Route} from "@angular/router";
import {Product} from "../../common/product";
import {CartItem} from "../../common/cart-item";
import {CartService} from "../../services/cart.service";

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  product: Product;


  constructor(private productService: ProductService, private route: ActivatedRoute, private cartService: CartService) {
    this.product = new Product();
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() =>
      this.getProductDetails()
    );
  }

  getProductDetails() {
    const hasProductId: boolean = this.route.snapshot.paramMap.has("id");
    if (hasProductId) {
      const productId: number = +this.route.snapshot.paramMap.get("id")!;
      this.productService.getProduct(productId).subscribe(
        data => this.product = data
      );
    }
  }

  addToCart(product: Product) {
    const cartItem = new CartItem(product);
    this.cartService.addToCart(cartItem);
  }
}
