import {Component, OnInit} from '@angular/core';
import {KeycloakService} from "keycloak-angular";
import {ActivatedRoute, Router} from "@angular/router";
import {KeycloakProfile} from "keycloak-js";

@Component({
  selector: 'app-login-status',
  templateUrl: './login-status.component.html',
  styleUrls: ['./login-status.component.css']
})
export class LoginStatusComponent implements OnInit {

  userObject!: KeycloakProfile;
  isAuthenticated: any;
  storage: Storage = sessionStorage;

  constructor(private route: ActivatedRoute,
              private router: Router, private keycloakService: KeycloakService) {
  }

  ngOnInit() {
    this.isLoggedIn();
  }

  logout() {
    this.keycloakService.logout("http://localhost:4200");
  }

  login() {
    this.keycloakService.login();
  }

  async isLoggedIn() {
    this.isAuthenticated = await this.keycloakService.isLoggedIn();
    if (this.isAuthenticated) {
      this.userObject = await this.keycloakService.loadUserProfile();
      this.storage.setItem("email", JSON.stringify(this.userObject.email));
    }
  }
}


