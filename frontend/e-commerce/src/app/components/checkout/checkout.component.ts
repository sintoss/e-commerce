import {Component, OnInit} from "@angular/core";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {CheckoutformService} from "../../services/checkoutform.service";
import {Country} from "../../common/Country";
import {State} from "../../common/State";
import {Luv2shopValidators} from "../../validators/luv2shop-validtors";
import {CartService} from "../../services/cart.service";
import {CheckoutService} from "../../services/checkout.service";
import {Router} from "@angular/router";
import {Order} from "../../common/Order";
import {OrderItem} from "../../common/OrderItem";
import {Purchase} from "../../common/Purchase";
import {render} from "creditcardpayments/creditCardPayments";

@Component({
  selector: "app-checkout",
  templateUrl: "./checkout.component.html",
  styleUrls: ["./checkout.component.css"],
})
export class CheckoutComponent implements OnInit {
  checkoutFormGroup!: FormGroup;
  totalPrice: number = 0;
  totalQuantity: number = 0;
  creditCardYears: number[];
  creditCardMonths: number[];
  countries: Country[];
  shippingAddressStates: State[];
  billingAddressStates: State[];
  storage: Storage = sessionStorage;

  constructor(
    private formBuilder: FormBuilder,
    private checkoutFormService: CheckoutformService,
    private cartService: CartService,
    private checkoutService: CheckoutService,
    private router: Router
  ) {
    this.creditCardYears = [];
    this.creditCardMonths = [];
    this.countries = [];
    this.shippingAddressStates = [];
    this.billingAddressStates = [];
    render({
      id: "#payments",
      currency: "USD",
      value: <string><unknown>this.totalPrice,
      onApprove: (details) => {
        "Transaction successful"
      }
    })
  }

  ngOnInit(): void {
    this.buildCheckoutForm();
    this.getCreditCardMonths();
    this.getCreditCardYears();
    this.getCountriesList();
    this.reviewCartDetails();
  }


  reviewCartDetails() {
    this.cartService.totalPrice.subscribe(data => this.totalPrice = data);
    this.cartService.totalQuantity.subscribe(data => this.totalQuantity = data);
  }

  getUserEmailFromBrowserStorage() {
    let email: string = JSON.parse(this.storage.getItem("email") ?? '');
    return email;
  }

  getCountriesList() {
    this.checkoutFormService
      .getCountries()
      .subscribe((data) => (this.countries = data));
  }

  getStatesByCountry(formGroupName: string) {
    const formGroup = this.checkoutFormGroup.get(formGroupName)!;

    const countryCode = formGroup.value.country.code;

    this.checkoutFormService
      .getStateByCountryCode(countryCode)
      .subscribe((data) => {
        if (formGroupName === "shippingAddress") {
          this.shippingAddressStates = data;
        } else {
          this.billingAddressStates = data;
        }

        // select first item by default
        formGroup.get("state")?.setValue(data[0]);
      });
  }

  getCreditCardMonths() {
    const startMonth: number = new Date().getMonth() + 1;
    this.checkoutFormService
      .getCreditCardMonths(startMonth)
      .subscribe((data) => (this.creditCardMonths = data));
  }

  getCreditCardYears() {
    this.checkoutFormService
      .getCreditCardYears()
      .subscribe((data) => (this.creditCardYears = data));
  }

  copyShippingAddressToBillingAddress(event: any) {
    if (event.target.checked) {
      this.checkoutFormGroup.controls["billingAddress"].setValue(
        this.checkoutFormGroup.controls["shippingAddress"].value
      );
      this.billingAddressStates = this.shippingAddressStates;
      this.checkoutFormGroup.controls["billingAddress"].disable();
    } else {
      this.checkoutFormGroup.controls["billingAddress"].enable();
      this.checkoutFormGroup.controls["billingAddress"].reset();
      this.billingAddressStates = [];
    }
  }

  buildCheckoutForm() {
    this.checkoutFormGroup = this.formBuilder.group({
      customer: this.formBuilder.group({
        firstName: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
        lastName: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
        email: new FormControl(this.getUserEmailFromBrowserStorage(), [
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        ]),
      }),
      shippingAddress: this.formBuilder.group({
        country: new FormControl("", [Validators.required]),
        street: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
        state: new FormControl("", [Validators.required]),
        city: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
        zipCode: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
      }),
      billingAddress: this.formBuilder.group({
        street: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
        city: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
        state: new FormControl("", [Validators.required]),
        country: new FormControl("", [Validators.required]),
        zipCode: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
      }),
      creditCard: this.formBuilder.group({
        cardType: new FormControl("", [Validators.required]),
        nameOnCard: new FormControl("", [
          Validators.required,
          Validators.minLength(2),
          Luv2shopValidators.notOnlyWhiteSpace,
        ]),
        cardNumber: new FormControl("", [
          Validators.required,
          Validators.pattern("[0-9]{16}"),
        ]),
        securityCode: new FormControl("", [
          Validators.required,
          Validators.pattern("[0-9]{3}"),
        ]),
        expirationMonth: new FormControl("", [Validators.required]),
        expirationYear: new FormControl("", [Validators.required]),
      }),
    });
  }

  onSubmit() {
    if (this.checkoutFormGroup.invalid) {
      this.checkoutFormGroup.markAllAsTouched();
      return;
    }

    let order = new Order();

    order.totalPrice = this.totalPrice;

    order.totalQuantity = this.totalQuantity;

    const cartItems = this.cartService.cartItems;

    let orderItems: OrderItem[];

    orderItems = cartItems.map(tempCartItem => new OrderItem(tempCartItem));

    let purchase = new Purchase();


    purchase.customer = this.checkoutFormGroup.controls['customer'].value;

    purchase.shippingAddress = this.checkoutFormGroup.controls['shippingAddress'].value;
    const shippingState: State = JSON.parse(JSON.stringify(purchase.shippingAddress.state));
    const shippingCountry: Country = JSON.parse(JSON.stringify(purchase.shippingAddress.country));
    purchase.shippingAddress.state = shippingState.name;
    purchase.shippingAddress.country = shippingCountry.name;


    purchase.billingAddress = this.checkoutFormGroup.controls['billingAddress'].value;
    const billingState: State = JSON.parse(JSON.stringify(purchase.billingAddress.state));
    const billingCountry: Country = JSON.parse(JSON.stringify(purchase.billingAddress.country));
    purchase.billingAddress.state = billingState.name;
    purchase.billingAddress.country = billingCountry.name;


    purchase.order = order;
    purchase.orderItems = orderItems;


    this.checkoutService.placeOrder(purchase).subscribe(
      {
        next: response => {
          this.resetCart();
          alert("your order has been received.\n Order tracking number" + response.orderTrackingNumber)
        },
        error: err => {
          alert("there was and error : " + err.message);
        }
      }
    );

  }

  // customer fields

  get firstName() {
    return this.checkoutFormGroup.get("customer.firstName");
  }

  get lastName() {
    return this.checkoutFormGroup.get("customer.lastName");
  }

  get email() {
    return this.checkoutFormGroup.get("customer.email");
  }

  // shipping address fields

  get shippingAddressStreet() {
    return this.checkoutFormGroup.get("shippingAddress.street");
  }

  get shippingAddressCity() {
    return this.checkoutFormGroup.get("shippingAddress.city");
  }

  get shippingAddressState() {
    return this.checkoutFormGroup.get("shippingAddress.state");
  }

  get shippingAddressCountry() {
    return this.checkoutFormGroup.get("shippingAddress.country");
  }

  get shippingAddressZipCode() {
    return this.checkoutFormGroup.get("shippingAddress.zipCode");
  }

  // billing address fields

  get billingAddressStreet() {
    return this.checkoutFormGroup.get("billingAddress.street");
  }

  get billingAddressCity() {
    return this.checkoutFormGroup.get("billingAddress.city");
  }

  get billingAddressState() {
    return this.checkoutFormGroup.get("billingAddress.state");
  }

  get billingAddressZipCode() {
    return this.checkoutFormGroup.get("billingAddress.zipCode");
  }

  get billingAddressCountry() {
    return this.checkoutFormGroup.get("billingAddress.country");
  }

  // credit card field

  get creditCardType() {
    return this.checkoutFormGroup.get("creditCard.cardType");
  }

  get creditCardNameOnCard() {
    return this.checkoutFormGroup.get("creditCard.nameOnCard");
  }

  get creditCardNumber() {
    return this.checkoutFormGroup.get("creditCard.cardNumber");
  }

  get creditCardSecurityCode() {
    return this.checkoutFormGroup.get("creditCard.securityCode");
  }

  handleMonthsAndYears() {

    const creditCardFormGroup = this.checkoutFormGroup.get("creditCard");
    const currentYear: number = new Date().getFullYear();
    const selectedYear: number = Number(
      creditCardFormGroup?.value.expirationYear
    );
    let startMonth: number;
    if (currentYear === selectedYear) {
      startMonth = new Date().getMonth() + 1;
    } else startMonth = 1;
    this.checkoutFormService
      .getCreditCardMonths(startMonth)
      .subscribe((data) => (this.creditCardMonths = data));
  }

  private resetCart() {
    this.cartService.cartItems = [];
    this.cartService.totalQuantity.next(0);
    this.cartService.totalPrice.next(0);
    this.checkoutFormGroup.reset();
    this.router.navigateByUrl("/products");
  }
}
