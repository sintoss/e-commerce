import {Injectable} from "@angular/core";
import {CartItem} from "../common/cart-item";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CartService {
  cartItems: CartItem[];
  totalPrice: BehaviorSubject<number>;
  totalQuantity: BehaviorSubject<number>;
  storage: Storage = localStorage;

  constructor() {
    this.cartItems = [];

    this.totalPrice = new BehaviorSubject<number>(0);
    this.totalQuantity = new BehaviorSubject<number>(0);
    // @ts-ignore
    let data = JSON.parse(this.storage.getItem('cartItems'));

    if (data != null) {
      this.cartItems = data;
      this.computeCartTotals();
    }
  }

  addToCart(cartItem: CartItem) {
    let alreadyExistsInCart: boolean = false;
    let existingCartItem: CartItem | undefined = undefined;

    if (this.cartItems.length > 0) {
      existingCartItem = this.cartItems.find(
        (tempCartItem) => tempCartItem.id === cartItem.id
      );
      alreadyExistsInCart = existingCartItem != undefined;
    }
    if (alreadyExistsInCart) existingCartItem!.quantity++;
    else this.cartItems.push(cartItem);

    this.computeCartTotals();
  }

  computeCartTotals() {
    let totalPriceValue = 0;
    let totalQuantityValue = 0;
    for (let currentCartItem of this.cartItems) {
      totalPriceValue += currentCartItem.quantity * currentCartItem.unitPrice;
      totalQuantityValue += currentCartItem.quantity;
    }
    this.totalPrice.next(totalPriceValue);
    this.totalQuantity.next(totalQuantityValue);
    this.persistsCartItems();
  }

  decrementQuantity(tempCartItem: CartItem) {
    tempCartItem.quantity--;
    if (tempCartItem.quantity === 0) this.remove(tempCartItem);
    else this.computeCartTotals();
  }

  remove(cartItem: CartItem) {
    const itemIndex = this.cartItems.findIndex(
      (tempCartItem) => tempCartItem.id === cartItem.id
    );
    if (itemIndex > -1) this.cartItems.splice(itemIndex, 1);
    this.computeCartTotals();
  }

  persistsCartItems() {
    this.storage.setItem("cartItems", JSON.stringify(this.cartItems));
  }


}
