import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Product} from "../common/product";
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly baseUrl = environment.baseUrl;
  private readonly searchUrl = this.baseUrl + "/products/search";

  constructor(private httpClient: HttpClient) {
  }

  getProductListPaginate(categoryId: number, page: number, pageSize: number): Observable<GetResponseProducts> {
    const searchUrl = this.searchUrl + "/findByCategoryId?id=" + categoryId + "&page=" + page + "&size=" + pageSize;
    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  searchForProductsPaginate(keyword: string, page: number, pageSize: number): Observable<GetResponseProducts> {
    const searchUrl = this.searchUrl + "/findByNameContainingIgnoreCase?name=" + keyword + "&page=" + page + "&size=" + pageSize;
    return this.httpClient.get<GetResponseProducts>(searchUrl);
  }

  getProduct(productId: number) {
    const productUrl = this.baseUrl + "/products/" + productId;
    return this.httpClient.get<Product>(productUrl);
  }

  private getProducts(searchUrl: string) {
    return this.httpClient.get<GetResponseProducts>(searchUrl).pipe(
      map(response => response._embedded.products)
    );
  }

  getProductList(categoryId: number): Observable<Product[]> {
    const searchUrl = this.searchUrl + "/findByCategoryId?id=" + categoryId;
    return this.getProducts(searchUrl);
  }

  searchForProducts(keyword: string, page: number, pageSize: number): Observable<Product[]> {
    const searchUrl = this.searchUrl + "/findByNameContainingIgnoreCase?name=" + keyword;
    return this.getProducts(searchUrl);
  }

}

interface GetResponseProducts {
  data: {
    products: Product[];
  },
  page: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number
  }
}



