import {Injectable} from '@angular/core';
import {map, Observable, of} from "rxjs";
import {Country} from "../common/Country";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {State} from "../common/State";

@Injectable({
  providedIn: 'root'
})
export class CheckoutformService {

  private readonly baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }


  getCountries(): Observable<Country[]> {
    return this.httpClient.get<GetResponseCountries>(this.baseUrl + "/countries").pipe(
      map(response => response._embedded.countries)
    );
  }

  getStateByCountryCode(countryCode: string): Observable<State[]> {
    const searchUrl = this.baseUrl + "/states/search/findByCountryCode?code=" + countryCode;
    return this.httpClient.get<GetResponseStates>(searchUrl).pipe(
      map(response => response._embedded.states)
    );
  }


  getCreditCardMonths(startMonth: number): Observable<number[]> {

    let data: number[] = [];

    for (let month = startMonth; month <= 12; month++) {
      data.push(month);
    }
    return of(data);
  }

  getCreditCardYears(): Observable<number[]> {
    let data: number[] = [];
    let startYear = new Date().getFullYear();
    let endYear = startYear + 10;
    for (let year = startYear; year <= endYear; year++) {
      data.push(year);
    }
    return of(data);
  }

}

interface GetResponseCountries {
  _embedded: {
    countries: Country[]
  }
}

interface GetResponseStates {
  _embedded: {
    states: State[]
  }
}
