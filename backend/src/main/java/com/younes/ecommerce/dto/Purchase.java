package com.younes.ecommerce.dto;


import com.younes.ecommerce.entity.Address;
import com.younes.ecommerce.entity.Customer;
import com.younes.ecommerce.entity.Order;
import com.younes.ecommerce.entity.OrderItem;
import lombok.Data;

import java.util.Set;

@Data
public class Purchase {
    
    private Customer customer;
    private Address shippingAddress;
    private Address billingAddress;
    private Order order;
    private Set<OrderItem> orderItems;
}
