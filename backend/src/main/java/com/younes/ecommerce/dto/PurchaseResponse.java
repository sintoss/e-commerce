package com.younes.ecommerce.dto;


public record PurchaseResponse(String orderTrackingNumber) {
}
