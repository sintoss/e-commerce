package com.younes.ecommerce.service;


import com.younes.ecommerce.dto.Purchase;
import com.younes.ecommerce.dto.PurchaseResponse;

public interface CheckoutService {

    PurchaseResponse placeOrder(Purchase purchase);
}
