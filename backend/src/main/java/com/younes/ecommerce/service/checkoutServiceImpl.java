package com.younes.ecommerce.service;

import com.younes.ecommerce.dao.CustomerRepository;
import com.younes.ecommerce.dto.Purchase;
import com.younes.ecommerce.dto.PurchaseResponse;
import com.younes.ecommerce.entity.Customer;
import com.younes.ecommerce.entity.Order;
import com.younes.ecommerce.entity.OrderItem;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;
import java.util.UUID;


@Service
public class checkoutServiceImpl implements CheckoutService {
    private final CustomerRepository customerRepository;

    public checkoutServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    @Transactional
    public PurchaseResponse placeOrder(Purchase purchase) {

        // retrieve the order info from dto
        Order order = purchase.getOrder();
        // generate tracking number
        String orderTrackingNumber = generateOrderTrackingNumber();
        order.setOrderTrackingNumber(orderTrackingNumber);
        // populate order with orderItems
        Set<OrderItem> orderItems = purchase.getOrderItems();
        orderItems.forEach(order::add);
        // populate order with billing and shipping address
        order.setShippingAddress(purchase.getShippingAddress());
        order.setBillingAddress(purchase.getBillingAddress());
        // populate order customer with order
        Customer customer = purchase.getCustomer();

        // check if this is an existing customer

        String email = customer.getEmail();
        Customer customerFromDB = customerRepository.findByEmail(email);

        if (customerFromDB != null)
            customer = customerFromDB;


        customer.add(order);
        // save to the database
        customerRepository.save(customer);
        // return a response
        return new PurchaseResponse(orderTrackingNumber);

    }

    private String generateOrderTrackingNumber() {

        // generate a random UUID number (UUID version-d)

        return UUID.randomUUID().toString();


    }
}
